;  Olive profile make file

core = 8.x
api = 2

; Structur
projects[paragraphs][subdir] = "contrib"
projects[paragraphs][version] = "1.0-rc4"

; Media
projects[entity_reference_revisions][subdir] = "contrib"
projects[entity_reference_revisions][version] = "1.0-rc6"
projects[media_entity][subdir] = "contrib"
projects[media_entity][version] = "1.0-beta2"
projects[media_entity_image][subdir] = "contrib"
projects[media_entity_image][version] = "1.0-beta2"
projects[video_embed_field][subdir] = "contrib"
projects[video_embed_field][version] = "1.0-rc8"

; Content authoring
projects[mollom] = "contrib"
projects[mollom] = "1.1"

; Development
projects[features][subdir] = "contrib"
projects[features][version] = "3.0-beta3"
projects[masquerade][subdir] = "devel"
projects[masquerade][version] = "1.0-beta1"

; Search
projects[search_api][subdir] = "contrib"
projects[search_api][version] = "1.0-alpha14"
projects[facets][subdir] = "contrib"
projects[facets][subdir] = "1.0-alpha2"
